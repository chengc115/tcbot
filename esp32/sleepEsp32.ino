#include <LiquidCrystal_I2C.h>
#include <pthread.h>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
///////////////////////////////////////////

///////////////////////////////////////////
#define LED_BUILTIN 2
#define BUTTON_BUILTIN 0
#define BLE_ScanTime (5)
LiquidCrystal_I2C lcd(0x3f, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

static BLEUUID serviceUUID("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
static BLEUUID   txUUID("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
static BLEUUID   rxUUID("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
// -----------------------------------------
static char *deviceName[] = {"none", "OBB2CC5", "O3EF03B", "S1F135A"};
uint8_t deviceMax = 3;
uint8_t deviceIndex = 0;
static int bleStat = 0;
static boolean deviceConnected = false;
///////////////////////////////////////////////

int16_t readStat = -1;
int16_t readIdx = -1;
int16_t readSize = 0;
int16_t readMax = -1;
int16_t readMemMax = -1;
/////////////////////////////////////////////
const uint32_t delayLcdThread = 1000;
TaskHandle_t tidLcdThread = 0;
uint32_t paramLcdThread = 0;

const uint32_t delaySerialThread = 300;
TaskHandle_t tidSerialThread = 0;
uint32_t paramSerialThread = 0;

const uint32_t delayBleThread = 300;
TaskHandle_t tidBleThread = 0;
uint32_t paramBleThread = 0;

const uint32_t delayChkThread = 3000;
TaskHandle_t tidChkThread = 0;
uint32_t paramChkThread = 0;


// -----------------------------------------
BLEClient*  pClient  = BLEDevice::createClient();
BLEScan* pBLEScan = NULL;
static BLEAdvertisedDevice* myDevice = NULL;
static BLERemoteCharacteristic* pRxRemote = NULL;
static BLERemoteCharacteristic* pTxRemote = NULL;

uint8_t notifyMsg[128];
int notifyLen = 0;

//------------------------
static uint8_t recvSn = 0xff;
static uint8_t waitSn = 0xff;
uint32_t cmdStart = 0;
uint32_t cmdEnd = 0;

//////////////////////////////
void pin_rising_interrupt(void) {
  if ( ++deviceIndex > deviceMax) deviceIndex = 0;
}


static char *HEX_TABLE = "0123456789abcdef";
void printHex(Stream &out, uint8_t *pData, int len) {
  for (int i = 0; i < len; i++) {
    uint8_t v;
    v = pData[i];
    out.print(HEX_TABLE[ (v & 0xf0) >> 4]);
    out.print(HEX_TABLE[ (v & 0x0f) >> 0]);
  }
}

///////////////////////////////////////
#define USING_WIFI 0

#if USING_WIFI

const uint32_t delayMqttThread = 300;
TaskHandle_t tidMqttThread = 0;
uint32_t paramMqttThread = 0;

#include "WiFi.h"

#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <WiFiClient.h>

WiFiUDP ntpUDP;
// NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 288000);
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 288000, 288000);

const char *STA_SSID = "TT49";
const char *STA_PASS = "22927399";

//const char *STA_SSID = "TVTC-MOA";
//const char *STA_PASS = "22927399";

// #define STA_SSID "TT49"
// #define STA_PASS "22927399"

IPAddress mqttServer(192, 168, 65, 65);
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void WiFiEvent(WiFiEvent_t event) {
  switch (event) {
    case SYSTEM_EVENT_STA_GOT_IP:
      Serial.print("STA IPv4: ");
      Serial.println(WiFi.localIP());
      break;
  }
}

void taskMqttThread(void * pvParameters ) {
  static uint32_t wakeupSignal;

  WiFi.onEvent(WiFiEvent);
  mqttClient.setServer(mqttServer, 1883);
  mqttClient.setCallback(mqttCallback);

  static uint8_t state = 0;
  uint32_t nextSleep = delayMqttThread;
  do {
    wakeupSignal = ulTaskNotifyTake(-1L, nextSleep);
    if (state > 1 && WiFi.status() != WL_CONNECTED) state = 0;
    if (state > 4 && !mqttClient.connected() ) state = 4;

    switch (state) {
      case 0:
        WiFi.begin(STA_SSID, STA_PASS);
        state++;
      case 1:
        nextSleep = 500;
        if (WiFi.status() == WL_CONNECTED) {
          state++;
        }
        Serial.print ( "." );
        break;
      case 2:
        timeClient.begin();
        nextSleep = 500;
        state++;
        break;
      case 3:
        nextSleep = 500;
        timeClient.update();
        state++;
        break;
      case 4:
        nextSleep = 500;
        mqttClient.connect("MYID");
        Serial.print("Attempting MQTT connection...");
        state++;
        break;
      case 5:
        nextSleep = 1000;
        if (mqttClient.connected()) {
          state++;
        }
        break;
      case 6:
        nextSleep = 3000;
        mqttClient.publish("outTopic", "my time:");
        break;
    }
  } while (pvParameters);
}

#endif
//////////////////////////////

char lcdLine[4][36];
void printLcd(void) {
  static char buf[24];
  uint8_t l;
  //  lcd.clear();
#if USING_WIFI
  timeClient.update();
  sprintf(lcdLine[0], "%02d:%02d:%02d %s%c%d/%d",
          timeClient.getHours(), timeClient.getMinutes(), timeClient.getSeconds(),
          deviceName[deviceIndex],
          ((deviceConnected) ? '*' : '?'),
          deviceIndex, bleStat);
#else
  sprintf(lcdLine[0], "%s%c%d/%d/%d^%d",
          deviceName[deviceIndex],
          ((deviceConnected) ? '*' : '?'),
          deviceIndex, bleStat, readStat,
          cmdEnd - cmdStart);
#endif
  // Serial.println(timeClient.getFormattedTime());

  if (!deviceConnected) {
    // lcd.clear();
    // lcd.setCursor(0, 0), lcd.print(lcdLine[0]);
    lcdLine[3][0] = lcdLine[2][0] = lcdLine[1][0] = lcdLine[1][0] = 0;
    for (int i = 0; i < deviceMax && i < 6; i++) {
      sprintf(buf, "%d:%s ", (i + 1), deviceName[i + 1]);
      strcat(lcdLine[(i / 2) + 1], buf);
    }

    for (int i = 0; i < 4; i++) {
      lcd.setCursor(0, i);
      lcd.printf("%-20s",lcdLine[i]);
    }
  } else
    for (int i = 0; i < 4; i++) {
      l = 0;
      while ( (buf[l] = lcdLine[i][l]) != 0 && l < 20) l++;
      while ( l < 20) (buf[l++] = 0x20); buf[l] = 0;

      lcd.setCursor(0, i);
      lcd.print(buf);
    }
}

void taskLcdThread(void * pvParameters ) {
  static uint32_t wakeupSignal;
  lcd.init();
  lcd.clear();
  // lcd.begin(20, 4);
  lcd.backlight();
  for (int i = 0; i < 4; i++) {
    strcpy(lcdLine[i], "hello");
  }

  lcd.home();

  do {
    wakeupSignal = ulTaskNotifyTake(-1L, delayLcdThread);
    printLcd();
  } while (pvParameters);
}
////////////////////////////////////////////////
static char serialLine[80];

void deviceChange(int x) {
  if (x != deviceIndex) {
    bleStat = 1; // reset all!
  }
  deviceIndex = x;
  //  sprintf(lcdLine[0], "%s %d %d", deviceName[deviceIndex], deviceIndex, bleIndex);
}

// ------------------------------------

void startReadSleepAll() {
  readStat = 0;
  readIdx = 0;
  readMax = 19;
  // paramChkThread = 1;
}


void startReadSleep(int idx) {
  readStat = 2;
  readIdx = idx;
  readMax = idx;
  // paramChkThread = 1;
}



void stopReadSleep() {
  readStat = -1;
  readIdx = -1;
}

void getSleepData() {
  static char cmd[20];
  static int nextMem = 0;
  static uint32_t startTs, endTs;
  switch (readStat) {
    case 0:
      Serial.printf("Try to get index:%d\n", readIdx);
      startTs = millis();
      sendCmd(pTxRemote, "get.sleepidx");
      readStat++;
      break;
    case 1:
      // Serial.printf("Get keyword ... :%d\n", readIdx);
      delay(1000);
      if (strstr((char *)notifyMsg + 2, "idx=") != NULL) {
        readMax = atoi((char *)notifyMsg + 6);
        if (readIdx < 20) {
          readStat++;
        } else {
          readStat = -1;
        }
      }
      break;
    case 2:
      sprintf(cmd, "SLEEP_HIST=%d", readIdx);
      sendCmd(pTxRemote, cmd);
      readStat++;
      break;
    case 3:
      if (deviceName[deviceIndex][0] == 'O' && strstr((char *)notifyMsg + 2, "ODI=") != NULL) {
        readStat++;
      }
      if (deviceName[deviceIndex][0] == 'S' && strstr((char *)notifyMsg + 2, "SLEEP_EFF=") != NULL) {
        readStat++;
      }
      if (strstr((char *)notifyMsg + 2, "MSG=") != NULL) {
        readStat = -1;
      }
      break;
    case 4:
      sprintf(cmd, "SLEEP_SIZE=%d", readIdx);
      sendCmd(pTxRemote, cmd);
      readStat++;
      break;
    case 5:
      // Serial.print("compare ..."); Serial.println((char *)notifyMsg);
      if (strstr((char *)notifyMsg + 2, "SLEEP_size=") != NULL) {
        readSize = atoi((char *)notifyMsg + 13);
        Serial.print("try to read data.... size= "); Serial.println(readSize);
        readStat++;
        nextMem = 0;
      }
      break;
    case 6:
      if (nextMem >= readSize) {
        readStat = 8;
        break;
      }
      sprintf(cmd, "SLEEP_MEM=%02d%04d", readIdx, nextMem);
      sendCmd(pTxRemote, cmd);
      readStat++;
      break;
    case 7:
      if (strstr((char *)notifyMsg + 2, "Index=") != NULL) {
        nextMem++;
        readStat = 6;
      }
      break;
    case 8:
      endTs = millis();
      Serial.printf("**** total read=%d/%d, time=%ld; %d, %d\n", nextMem, readSize , (endTs - startTs), readIdx, readMax);
      //Serial.print(
      readIdx++;
      if ( readIdx > readMax) {
        readStat = -1;
      } else {
        readStat = 2;
      }
    default:
      break;
  }
}


void taskChkThread(void * pvParameters ) {
  static uint32_t wakeupSignal;
  long v;
  if (pvParameters != NULL) readStat = -1;
  do {
    wakeupSignal = ulTaskNotifyTake(-1L, (readStat == -1) ? delayChkThread : 200);
    if (readStat != -1) {
      getSleepData();
    }
  } while (pvParameters != NULL);
}



// ------------------------------------
void serialCallback(char *line) {
  sprintf(lcdLine[1], "%02x %s", waitSn, line);
  lcdLine[1][20] = 0;
  // strncpy(lcdLine[1], line, 20);


  if (strncmp("restart", line, 7) == 0 || strncmp("reset ", line, 6) == 0) {
    Serial.println("\n**** cmd restart ***"); delay(3000);
    ESP.restart();

  } else if (strncmp("conn ", line, 5) == 0) {
    int8_t x;
    if ( (x = line[5]) == 0 ) return;
    x -= '0';
    if (x < 0) x = 0;
    if (x > deviceMax) x = deviceMax;
    deviceChange(x);
    return;
  } else if (strncmp("sleep ", line, 6) == 0 ) {
    int sleepIdx = atoi(line + 6);
    Serial.print("********** sleep: try to read:"); Serial.println(sleepIdx);
    startReadSleep(sleepIdx);
  } else if (strncmp("sleepall", line, 8) == 0 ) {
    int sleepIdx=0;
    Serial.print("********** sleep: try to read:"); Serial.println(sleepIdx);
    startReadSleepAll();
  } else if (strncmp("stop", line, 4) == 0) {
    stopReadSleep();
  } else {
    if (pTxRemote) {
      sendCmd(pTxRemote, (char *)line);
    }
  }
}


//////////////////////////////////////////////////////
void taskSerial(Stream & stream) {
  static char line[128];
  static uint8_t ch;
  static int index = 0;
  boolean nl = false;

  while (stream.available()) {
    ch = stream.read();
    if (ch >= 0x20 && ch <= 0x7f) {
      line[index] = ch;
      index++;
    }
    if (ch == 0x0d || ch == 0x0a || index >= 100) {
      line[index] = 0;
      nl = true;
      break;
    }
  }
  if (nl) {
    serialCallback(line);
    nl = false;
    index = 0;
  }
}



void taskSerialThread(void * pvParameters ) {
  static uint32_t wakeupSignal;
  do {
    wakeupSignal = ulTaskNotifyTake(-1L, delaySerialThread);
    taskSerial(Serial);
  } while (pvParameters);
}
////////////////////////////////////////////////////


class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      Serial.printf("Advertised Device: [ % s]\n", advertisedDevice.toString().c_str());
      if (deviceIndex > 0 && deviceName[deviceIndex] != NULL) {
        if (advertisedDevice.getName().compare(deviceName[deviceIndex]) == 0) {
          Serial.printf("                          ^^^^^^^");
          if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)
             ) {
            Serial.printf("                                           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            myDevice = new BLEAdvertisedDevice(advertisedDevice);
          } else {
            Serial.println("**** connect connect to serviceid ***"); delay(3000);
            ESP.restart();
          }
          Serial.println();
        }
      }
    }
};
MyAdvertisedDeviceCallbacks *deviceCallback = new MyAdvertisedDeviceCallbacks();

// ---------------------------------------------------

class MyClientCallback : public BLEClientCallbacks {
    void onConnect(BLEClient* pclient) {
      deviceConnected = true;
    }

    void onDisconnect(BLEClient* pclient) {
      deviceConnected = false;
      bleStat = 1;
    }
};
MyClientCallback *clientCallback = new MyClientCallback();


static void notifyCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,  size_t msgLen,  bool isNotify) {

  cmdEnd = millis();
  //  Serial.print("Notify callback for characteristic ");
  //  Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
  //  Serial.print(" of data length ");
  //  Serial.println(length);
  if (msgLen == 0) return;
  if ( strcmp((char *)pData + 2, "BAT=") != 0) {
    memcpy(notifyMsg, pData, msgLen); 
    notifyMsg[notifyLen = msgLen] = 0;
  }
  static int cs = 0;
  static int len = 0;
  cs = pData[msgLen - 1];
  len = msgLen - 3;
  pData[msgLen - 1] = 0;
  Serial.printf("read__ <<< % 3d, % 02x, % 3d, % 02x; [ % s] ; hex = ", pData[0], pData[1],
                len, cs, (char*)(pData + 2));
  printHex(Serial, pData, msgLen);
  Serial.println("");

  if (msgLen > 26) pData[26] = 0;

  recvSn = pData[0];

  static uint8_t lastSn = 0xaa;

  if (recvSn == waitSn) {
    if (lastSn != recvSn) {
      sprintf(lcdLine[2], " % 02x % s", recvSn, (char *)(pData + 2));
      lastSn = recvSn;
    } else {
      sprintf(lcdLine[3], " % 02x % s", recvSn, (char *)(pData + 2));
    }
  } else {
    sprintf(lcdLine[3], " % 02x % s", recvSn, (char *)(pData + 2));
  }

  //  printHex(Serial, pData, length);
  //  Serial.print("  ");
  //  pData[length - 1] = 0;
  //  Serial.println((char*)pData + 2);
}

// ---------------------------------------------------
void taskBleThread(void * pvParameters ) {
  static uint32_t wakeupSignal;
  static BLERemoteService* pRemoteService;
  BLEScanResults foundDevices ;
  if (pvParameters != NULL) bleStat = 0;

  Serial.println("Start bluetooth task");
  do {
    wakeupSignal = ulTaskNotifyTake(-1L, delayBleThread);
    switch (bleStat) {
      case 0: // init ble device
        BLEDevice::init("");
        pBLEScan = BLEDevice::getScan(); //create new scan
        pBLEScan->setAdvertisedDeviceCallbacks(deviceCallback);
        pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
        pBLEScan->setInterval(100);
        pBLEScan->setWindow(99);  // less or equal setInterval value
        bleStat++;
        break;
      case 1: // clean all variable
        Serial.println("Reset my device done....");
        if (pRxRemote != NULL) pRxRemote = NULL;
        if (pTxRemote != NULL) pTxRemote = NULL;
        if (pClient && pClient->isConnected()) {
          pClient->disconnect();
          // pClient = null;
        }
        if (myDevice) {
          delete myDevice;
          myDevice = NULL;
        }
        bleStat++;
        break;
      case 2: // search device!!
        // if (myDevice!=false) delete myDevice;
        foundDevices = pBLEScan->start(BLE_ScanTime, false);
        Serial.print("Devices found : ");
        Serial.println(foundDevices.getCount());
        Serial.print("Heap size = "); Serial.println(xPortGetFreeHeapSize());
        Serial.print("Scan done! deviceindex = "); Serial.print(deviceIndex); Serial.print("; name = "); Serial.println(deviceName[deviceIndex]);
        // pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
        if (myDevice != NULL) {
          Serial.println("-------------------------------------------- -");
          Serial.print("*** Found device : ");
          Serial.println(myDevice->getName().c_str());
          bleStat++;
          delay(1000);
        } else {
          delay (5000);
          break;
        }
      case 3: // try to connect!!
        Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Serial.print("Is connected ? "); Serial.println(pClient->isConnected());
        Serial.print("Forming a connection to ");
        Serial.println(myDevice->getAddress().toString().c_str());
        Serial.println(" - Created client ");
        pClient->setClientCallbacks(clientCallback);
        pClient->connect(myDevice);  // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)
        Serial.println(" - Connected to server");
        bleStat++;
        break;
      case 4: // wait until connected, print connect info.
        if (!pClient->isConnected()) break;
        pRemoteService = pClient->getService(serviceUUID);

        if (pRemoteService != nullptr) {
          Serial.println("------------------");
          std::map<std::string, BLERemoteCharacteristic*>* smap = pRemoteService->getCharacteristics();
          for (const auto &p : *smap) {
            Serial.print("\t");
            Serial.print(p.first.c_str());
            Serial.println();
          }
          Serial.println("-------------------------------------- -");
        }
        bleStat++;
        break;
      case 5: // has RemoteService or reset !!
        if (pRemoteService == nullptr) {
          // reset!!
          pClient->disconnect();
          bleStat = 1;
          break;
        }
        bleStat++;
      // conntinue execute

      case 6: // has pRxRemote? or reset!!
        pRxRemote = pRemoteService->getCharacteristic(rxUUID);
        if (pRxRemote == nullptr) {
          Serial.print("Failed to find our characteristic UUID : ");
          Serial.println(rxUUID.toString().c_str());
          bleStat = 1;
          break;
        }
        Serial.print(" - Found our characteristic : ");
        Serial.println(rxUUID.toString().c_str());
        // Read the value of the characteristic.
        if (pRxRemote->canRead()) {
          std::string value = pRxRemote->readValue();
          Serial.print("The characteristic value was : ");
          Serial.println(value.c_str());
        }

        if (pRxRemote->canNotify()) {
          Serial.println("Set Notify vcallback....");
          pRxRemote->registerForNotify(notifyCallback);
        }

        bleStat++;
      case 7: // has TXRemove or reset!!
        pTxRemote = pRemoteService->getCharacteristic(txUUID);
        if (pTxRemote == nullptr) {
          Serial.print("Failed to find our characteristic UUID : ");
          Serial.println(txUUID.toString().c_str());
          bleStat = 1;
          break;
        }
        Serial.print(" - Found our characteristic : ");
        Serial.println(txUUID.toString().c_str());

        // Read the value of the characteristic.
        if (pTxRemote->canRead()) {
          std::string value = pTxRemote->readValue();
          Serial.print("The characteristic value was : ");
          Serial.println(value.c_str());
        }

        if (pTxRemote->canNotify()) {
          Serial.println("Set Notify vcallback....");
          //    pTxRemote->registerForNotify(notifyCallback);
        }
        bleStat++;
      case 8:
        Serial.println("Connect READY");
        bleStat++;

      case 9:
        digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
        if ( (waitSn & 0x7f) == 0x7f) {
          sendCmd(pTxRemote, "cmd.pair,");
        }
        // if (pRxRemote->canRead()) {
        //   Serial.print("The characteristic value was : ");
        // }
        //        while (pRxRemote->canRead()) {
        //          Serial.println(value, HEX);
        //        }
    }

  } while (pvParameters);
}

///////////////////////////////////////
void setup() {
  Serial.begin(115200);
  // Serial.begin(19200);
  // Serial.begin(57600);
  // delay(300);

  Serial.println("Hello");
  {
    attachInterrupt(BUTTON_BUILTIN, pin_rising_interrupt, RISING);
  }
  {
#if USING_WIFI
    WiFi.begin();
#endif
    btStart();
  }
  {
    xTaskCreatePinnedToCore(taskLcdThread, "lcdThread", 4000, &paramLcdThread, tskIDLE_PRIORITY, &tidLcdThread, 1);
    xTaskCreatePinnedToCore(taskSerialThread, "serialThread", 4000, &paramSerialThread, tskIDLE_PRIORITY, &tidSerialThread, 1);
    xTaskCreatePinnedToCore(taskBleThread, "bleThread", 20000, &paramBleThread, tskIDLE_PRIORITY, &tidBleThread, 0);
    xTaskCreatePinnedToCore(taskChkThread, "chkThread", 20000, &paramChkThread, tskIDLE_PRIORITY, &tidChkThread, 0);
#if USING_WIFI
    xTaskCreatePinnedToCore(taskMqttThread, "mqttThread", 10000, &paramMqttThread, tskIDLE_PRIORITY, &tidMqttThread, 0);
#endif
  }
  pinMode(LED_BUILTIN, OUTPUT);

}
////////////////////////////////////////

void sendCmd(BLERemoteCharacteristic *pRemote, char *st) {
  static char cmd[32] = {0};
  if (pRemote == NULL) return;
  uint8_t chkSum;

  int len = strlen(st);

  cmd[0] = (cmd[0] + 1) & 0x7f;
  cmd[1] = 0x80 | 0x02;
  strcpy((char *)(cmd + 2), st);
  if (cmd[len + 1] != ',') {
    cmd[len + 2] = ',';
    cmd[len + 3] = 0;
    len++;
  }
  chkSum = 0;
  for (int i = 0; i < len + 1; i++) {
    chkSum += cmd[i + 1];
  }

  // Serial.print("send out.."); Serial.println((char *)cmd);
  Serial.printf("write_ >>> % 3d, % 02x, % 3d, % 02x; [ % s]\n", cmd[0], cmd[1],
                len, chkSum,  (char*)(cmd + 2)  );
  // Serial.printf("write_ >>> % 3d, % 02x, [ % s] % d, % 02x\n", cmd[0], cmd[1], (char*)(cmd + 2), len, cs);
  cmd[len + 2] = chkSum;
  cmdStart = millis();
  pRemote->writeValue(cmd, len + 3);
  delay(30);
  waitSn = cmd[0]; recvSn = -1;

  sprintf(lcdLine[1], " % 02x % -20s", waitSn, st);
}
//////////////////////////////////////////////////
void loop() {
  // put your main code here, to run repeatedly:
  delay(3000);
  // mqttClient.loop();
  //  for (int i = 2; i < 4; i++) {
  //    sprintf(lcdLine[i], " % d", millis());
  //    delay(10);
  //  }
  //   digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  // deviceIndex = 1;
  // Serial.println("main...");
}
