#include <pthread.h>
#ifdef ESP32
#include <WiFi.h>
#include <NTPClient.h>
#endif
// ---------------------
#include <ros.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/String.h>


// -----------------------------
boolean pidInputChanged = false;
boolean dmpReady = false;
boolean wifiReady = false;
volatile boolean i2cLocked = false;
double lWheelAdd = 0;
double rWheelAdd = 0;


////////////////////////////////////////////////////////
#include "PID_v1.h"
#include "LMotorController.h"
const uint32_t motorTaskDelay = 100;
TaskHandle_t motorTaskTid = 0;
uint32_t motorTaskParam = 0;


//---------------------------

int ENA = 36;
int IN1 = 39;
int IN2 = 34;
int IN3 = 35;
int IN4 = 32;
int ENB = 33;

// WHAT IS PWM control!?
LMotorController motorController(ENA, IN1, IN2, ENB, IN3, IN4, 0.6, 1);

// -----------

#define MIN_ABS_SPEED 30
#define CONST_ORIGINAL_SETPOINT 97.28f

// #define CONST_PID_P 16.0
// #define CONST_PID_P 10.0
// #define CONST_PID_I 0.02
// #define CONST_PID_D 0.8


// #define CONST_PID_P 16.0
#define CONST_PID_P 25.0
#define CONST_PID_I 0.02
#define CONST_PID_D 0.8


// double kp , ki, kd;
// double prevKp, prevKi, prevKd;


double pidOutput = 0;
double originalSetpoint = (CONST_ORIGINAL_SETPOINT);
double pidInput = CONST_ORIGINAL_SETPOINT;
double setpoint = (CONST_ORIGINAL_SETPOINT);

PID pid(&pidInput, &pidOutput, &setpoint, CONST_PID_P, CONST_PID_I, CONST_PID_D, AUTOMATIC);

void motorTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t currentMillis;
  static uint32_t  ts1Hz = 0;
  static uint32_t  ts5Hz = 0;
  static double avg;

  do {
    wakeupSignal = ulTaskNotifyTake(-1L, motorTaskDelay);
    switch (state) {
      case 0:
        if (dmpReady == true && pidInputChanged == true) state++;
        break;
      case 1:
        pid.SetOutputLimits(-255, 255);
        pid.SetControllerDirection(DIRECT);
        // pid.SetControllerDirection(REVERSE);
        pid.SetMode(AUTOMATIC);
        // pid.SetSampleTime(100);
        state++;
      // break;
      case 2:

        pidInputChanged = false;
        if (avg - pidInput > 3 || avg - pidInput < -3) avg = pidInput;
        avg = avg * 0.9 + pidInput * 0.1;

        // pidInput=97.3+ (float)(random(-100, 100))/50.0f;

        pid.Compute();
        // Serial.printf("pid avg=%8.3f, in=%8.3f, out=%8.3f,set=%8.3f\n", avg, pidInput, pidOutput, setpoint);
        // digitalWrite(LED_BUILTIN, ((pidOutput > 0) ? HIGH : LOW));
        motorController.move(pidOutput, pidOutput, MIN_ABS_SPEED);

        unsigned long currentMillis = millis();
        if (currentMillis >  ts1Hz) {
          Serial.printf("pid avg=%8.3f, in=%8.3f, out=%8.3f,set=%8.3f\n", avg, pidInput, pidOutput, setpoint);
          loopAt1Hz();
          ts1Hz = currentMillis + 1000;
        }
        if (currentMillis >  ts5Hz) {
          loopAt5Hz();
          ts5Hz = currentMillis + 5000;
        }
    }

  } while (pvParam != NULL);
}

void loopAt1Hz() {
}


void loopAt5Hz() {
}


///////////////////////////////////////////////////////////////////////////////////
const uint32_t mpuTaskDelay = 10;
TaskHandle_t mpuTaskTid = 0;
uint32_t mpuTaskParam = 0;

#define MPU6050_DMP_FIFO_RATE_DIVISOR 0x09 // defined in MPU6050_6Axis_MotionApps20.h (200/(0x09+1))Hz

#include "MPU6050_6Axis_MotionApps20.h"

// ------------------------------
MPU6050 mpu;
#define MPU_INTERRUPT_PIN 2

uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[1050]; // FIFO storage buffer
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU

// volatile bool dmpReady = false;  // set true if DMP init was successful
//-----------------
void mpuPowerOn() {
  pinMode(5, OUTPUT);
  digitalWrite(5, HIGH);
  delay(1000);
  digitalWrite(5, LOW);
  delay(100);
}
// volatile uint8_t mpuInterrupt = 0;     // indicates whether MPU interrupt pin has gone high

static volatile uint8_t mpuInterrupt = 0;
void IRAM_ATTR  dmpDataReady() {
  ++mpuInterrupt;
}

void inline setGyroOffsets() {
  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
}
// -------------------------------
void mpuTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  static uint8_t readCount = 0;
  static uint8_t entryI2c = false;
  taskDelay = mpuTaskDelay;
  do {
    if (entryI2c) entryI2c = i2cLocked = false;
    wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    if (i2cLocked) {
      taskDelay = 5;
      continue;
    }
    entryI2c = i2cLocked = true;
    taskDelay = mpuTaskDelay;

    switch (state) {
      case 0:
        mpuPowerOn();
        mpu.initialize();
        // pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
        // pinMode(MPU_INTERRUPT_PIN, INPUT_PULLDOWN);
        pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
        delay(100);
        Serial.println(F("Testing device connections..."));
        Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
        Serial.println(F("\nSend any character to begin DMP programming and demo: "));
        Serial.println(F("Initializing DMP..."));
        devStatus = mpu.dmpInitialize();

        // supply your own gyro offsets here, scaled for min sensitivity
        setGyroOffsets();

        // make sure it worked (returns 0 if so)
        if (devStatus == 0) {
          // turn on the DMP, now that it's ready
          Serial.println(F("Enabling DMP..."));
          mpu.setDMPEnabled(true);

          // enable Arduino interrupt detection
          Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
          Serial.print(digitalPinToInterrupt(MPU_INTERRUPT_PIN));
          // Serial.print((MPU_INTERRUPT_PIN));
          Serial.println(F(")..."));
          attachInterrupt(digitalPinToInterrupt(MPU_INTERRUPT_PIN), dmpDataReady, RISING);
          mpuIntStatus = mpu.getIntStatus();

          // set our DMP Ready flag so the main loop() function knows it's okay to use it
          Serial.println(F("DMP ready! Waiting for first interrupt..."));
          dmpReady = true;

          // get expected DMP packet size for later comparison
          packetSize = mpu.dmpGetFIFOPacketSize();
        } else {
          // ERROR!
          // 1 = initial memory load failed
          // 2 = DMP configuration updates failed
          // (if it's going to break, usually the code will be 1)
          Serial.print(F("DMP Initialization failed (code "));
          Serial.print(devStatus);
          Serial.println(F(")"));
        }
        readCount = 0;
        state++;
      //break;
      case 1:
        if (devStatus == 0) {
          state++;
        } else {
          if (++readCount > 10) state = 0;
          break;
        }
      case 2: // check if no data...
        if (!dmpReady) {
          state = 0;
          break;
        }
        if (!mpuInterrupt) break;
        fifoCount = mpu.getFIFOCount();
        if ((fifoCount = mpu.getFIFOCount()) < packetSize) break;
        mpuIntStatus = mpu.getIntStatus();
        if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
          // overflow...
          mpu.resetFIFO();
          // fifoCount = mpu.getFIFOCount();
          Serial.printf("FIFO overflow: %04x, %04x, %d\n", mpuIntStatus, MPU6050_INTERRUPT_FIFO_OFLOW_BIT, fifoCount);
          // Serial.println(F("FIFO overflow!"));
          // state = 0;
          break;
        }
        state++;
      case 3:
        // while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
        readCount = 0;
        while (fifoCount >= packetSize) {
          // delay(1);
          mpu.getFIFOBytes(fifoBuffer, packetSize);
          fifoCount -= packetSize;
          readCount++;
          // fifoCount = mpu.getFIFOCount();
        } // we only want the latest value

        if (fifoCount != 0) {
          mpu.resetFIFO();
          break;
        }

        static Quaternion q;           // [w, x, y, z]         quaternion container

        static VectorFloat gravity;    // [x, y, z]            gravity vector
        static float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        pidInput = (ypr[1] * (180.0f / M_PI) + 270.0f);
        if (pidInput > 360.0f)pidInput -= 360.0f;
        pidInputChanged = true;

        static uint8_t lazy = 0;
        if ( (++lazy) > 0) {
          lazy = 0;
          if (1 == 2) {
            Serial.printf("ypr:\t%02d\t%8.3f\t%8.3f\t%8.3f\t%8.3f\n",
                          readCount, pidInput,
                          ypr[0] * (180.0 / M_PI),
                          ypr[1] * (180.0 / M_PI),
                          ypr[2] * (180.0 / M_PI)
                         );
          }
          if (1 == 2) {
            Serial.printf("quat: %2d, %8.3f\t%8.3f\t%8.3f\t%8.3f\t%8.3f\n", readCount, q.w, q.x, q.y, q.z, pidInput);
          }
        }
    }
    state = 2; // continue check data

  } while (pvParam != NULL);
}

/////////////////////////////////////////////
const uint32_t rosTaskDelay = 1000;
TaskHandle_t rosTaskTid = 0;
uint32_t rosTaskParam = 0;

// -----------------------------


void servo_cb(const std_msgs::UInt16& cmd_msg) {
  // servo.write(cmd_msg.data); //set servo angle, should be from 0-180
  Serial.println(cmd_msg.data);
  // digitalWrite(13, HIGH - digitalRead(13)); //toggle led
}

// -------------------
ros::NodeHandle  nh;
ros::Subscriber<std_msgs::UInt16> sub("servo", servo_cb);
// IPAddress rosServer(192, 168, 1, 1);
IPAddress rosServer(192, 168, 65, 65);

const uint16_t rosServerPort = 11411;

std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);

char hello[13] = "hello world!";

// ---------------------

ros::Subscriber<std_msgs::UInt16> rosSub("servo", servo_cb);

void rosTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  taskDelay = rosTaskDelay;
  do {
    wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    taskDelay = rosTaskDelay;
    switch (state) {
      case 0:
        if (wifiReady) state++;
        break;
      case 1:
        nh.getHardware()->setConnection(rosServer, rosServerPort);
        nh.initNode();
        Serial.print("IP = ");
        Serial.println(nh.getHardware()->getLocalIP());
        state++;
        break;
      case 2:
        nh.advertise(chatter);
        nh.spinOnce();
        break;
    }
  } while (pvParam != NULL);
}


// rosrun rosserial_python serial_node.py tcp
/// opt/ros/kinetic/share/turtlebot_gazebo
// roslaunch turtlebot_gazebo turtlebot_world.launch 
//  roslaunch turtlebot_teleop keyboard_teleop.launch 


/////////////////////////////////////////////
const uint32_t wifiTaskDelay = 1000;
TaskHandle_t wifiTaskTid = 0;
uint32_t wifiTaskParam = 0;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 28800, (8 * 60 * 60 * 100));

const char *ap_ssid[] = {"TT49", "71216457", "MOA-TC", "TVTC-INCUBATOR"};
const char *ap_pwd[] = {"22927399", "22611182", "22927399", "22927399"};
uint8_t ap_idx = 0;
// -------------------------------
void wifiTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  taskDelay = wifiTaskDelay;
  do {
    wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    taskDelay = wifiTaskDelay;
    switch (state) {
      case 0:
        Serial.printf("Try to connect %s\n", ap_ssid[ap_idx]);
        WiFi.begin(ap_ssid[ap_idx], ap_pwd[ap_idx]);
        state++;
        break;
      case 1:
        if (WiFi.status() == WL_CONNECTED) {
          Serial.println(WiFi.localIP());
          wifiReady = true;
          state++;
        }
        break;
      case 2:
        timeClient.begin();
        state++;
        break;
      case 3:
        if (timeClient.update()) {
          Serial.printf("now=%02d:%02d:%02d\n",  timeClient.getHours(),  timeClient.getMinutes(),  timeClient.getSeconds());
        }
        taskDelay = 5000;
        state++;
        break;
      case 4:
        rosTaskCode(NULL);
    }
  } while (pvParam != NULL);
}


////////////////////////////////////////////////////////
const uint32_t lcdTaskDelay = 1000;
TaskHandle_t lcdTaskTid = 0;
uint32_t lcdTaskParam = 0;
//-----------------------------------
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3f, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display
// LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

//---------------------------
char lcdLine[4][32];

void lcdTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  static char buf[32];
  static uint32_t loops = 0;
  static uint8_t entryI2c = false;
  taskDelay = lcdTaskDelay;
  do {
    if (entryI2c) entryI2c = i2cLocked = false;
    wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    if (i2cLocked) {
      taskDelay = 5;
      continue;
    }
    entryI2c = i2cLocked = true;
    taskDelay = lcdTaskDelay;

    switch (state) {
      case 0:
        lcd.init();
        // lcd.begin(20, 4);
        lcd.backlight();
        state++;
        break;
      case 1:
        lcd.clear();
        for (int i = 0; i < 4; i++) {
          strcpy(lcdLine[i], "hello");
        }
        state++; break;
      case 2:
        lcd.setCursor(6, 0);
        lcd.print(lcdLine[0]);
        state++;
        break;
      case 3:
        sprintf(lcdLine[1], "count=%ld", loops++);
        lcd.setCursor(0, 1);
        lcd.print(lcdLine[1]);
    }

  } while (pvParam != NULL);
}

//////////////////////////////////////
void setup() {
  Serial.begin(115200);
  delay(100);
  Wire.begin();

  Serial.println(F("Initializing I2C devices..."));
  Serial.println(__DATE__ "  " __TIME__);
  delay(100);
  xTaskCreatePinnedToCore(motorTaskCode, "motor", 4000, &motorTaskParam, tskIDLE_PRIORITY, &motorTaskTid, 1);
  xTaskCreatePinnedToCore(wifiTaskCode, "wifi", 16384, &wifiTaskParam, tskIDLE_PRIORITY, &wifiTaskTid, 0);
  xTaskCreatePinnedToCore(mpuTaskCode, "mpu", 16384, &mpuTaskParam, tskIDLE_PRIORITY, &mpuTaskTid, 1);
  xTaskCreatePinnedToCore(lcdTaskCode, "lcd", 4096, &lcdTaskParam, tskIDLE_PRIORITY, &lcdTaskTid, 1);
  //   xTaskCreatePinnedToCore(rosTaskCode, "ros", 40960, &rosTaskParam, tskIDLE_PRIORITY, &lcdTaskTid, 1);

}

void loop() {
  delay(1000);
  // put your main code here, to run repeatedly:

}