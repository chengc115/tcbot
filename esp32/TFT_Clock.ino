/*
  An example analogue clock using a TFT LCD screen to show the time
  use of some of the drawing commands with the library.

  For a more accurate clock, it would be better to use the RTClib library.
  But this is just a demo.

  This sketch uses font 4 only.

  Make sure all the display driver and pin comnenctions are correct by
  editting the User_Setup.h file in the TFT_eSPI library folder.

  #########################################################################
  ###### DON'T FORGET TO UPDATE THE User_Setup.h FILE IN THE LIBRARY ######
  #########################################################################

  Based on a sketch by Gilchrist 6/2/2014 1.0
*/

#include <SPI.h>
#include <TFT_eSPI.h> // Hardware-specific library
#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 20, 4);

#define LED_BUILTIN 2
#define BUTTON_BUILTIN 0

#define TFT_GREY 0x5AEB

TFT_eSPI tft = TFT_eSPI();       // Invoke custom library

float sx = 0, sy = 1, mx = 1, my = 0, hx = -1, hy = 0;    // Saved H, M, S x & y multipliers
float sdeg = 0, mdeg = 0, hdeg = 0;
uint16_t osx = 120, osy = 120, omx = 120, omy = 120, ohx = 120, ohy = 120; // Saved H, M, S x & y coords
uint16_t x0 = 0, x1 = 0, yy0 = 0, yy1 = 0;
uint32_t targetTime = 0;                    // for next 1 second timeout

static uint8_t conv2d(const char* p); // Forward declaration needed for IDE 1.6.x
uint8_t hh = conv2d(__TIME__), mm = conv2d(__TIME__ + 3), ss = conv2d(__TIME__ + 6); // Get H, M, S from compile time

boolean initial = 1;

// #define LED_BUILTIN 13

//　const char *ssid     = "TVTC-MOA";
// const char *ssid     = "TT49";
// const char *password = "22927399";


const char *ap_ssid[]={"TT49","71216457"};
const char *ap_pwd[]={"22927399","22611182"};
uint8_t ap_idx=1;
// const char *ssid     = "71216457";
// const char *password = "22611182";

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 28800, (8 * 60 * 60 * 100));

void initWiFi() {
//  WiFi.begin(ssid, password);
  WiFi.begin(ap_ssid[ap_idx], ap_pwd[ap_idx]);
  pinMode(LED_BUILTIN,OUTPUT);
  uint8_t x=0;
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    // Serial.print ( "." );
    Serial.print (x );
    if(x) x=0; else x=1;
    digitalWrite(LED_BUILTIN,(x)?HIGH:LOW);
  }

  timeClient.begin();
}



void setup(void) {
  delay(3000);
  Serial.begin(115200);
  lcd.init();
  lcd.backlight();
  lcd.home();

  Serial.println("Start ....");
  Serial.print("TFT_SCLK="); Serial.println(TFT_SCLK);
  Serial.print("MOSI="); Serial.println(TFT_MOSI);
  Serial.print("TFT_RST="); Serial.println(TFT_RST);
  Serial.print("TFT_DC="); Serial.println(TFT_DC);
  Serial.print("TFT_BLK="); Serial.println("not defined");
  Serial.print("MISO="); Serial.println(TFT_MISO);
  Serial.print("TFT_CS="); Serial.println(TFT_CS);

  tft.init();
  tft.setRotation(0);

  //tft.fillScreen(TFT_BLACK);
  //tft.fillScreen(TFT_RED);
  //tft.fillScreen(TFT_GREEN);
  //tft.fillScreen(TFT_BLUE);
  //tft.fillScreen(TFT_BLACK);
  tft.fillScreen(TFT_GREY);

  tft.setTextColor(TFT_WHITE, TFT_GREY);  // Adding a background colour erases previous text automatically

  // Draw clock face
  tft.fillCircle(120, 120, 118, TFT_GREEN);
  tft.fillCircle(120, 120, 110, TFT_BLACK);

  // Draw 12 lines
  for (int i = 0; i < 360; i += 30) {
    sx = cos((i - 90) * 0.0174532925);
    sy = sin((i - 90) * 0.0174532925);
    x0 = sx * 114 + 120;
    yy0 = sy * 114 + 120;
    x1 = sx * 100 + 120;
    yy1 = sy * 100 + 120;

    tft.drawLine(x0, yy0, x1, yy1, TFT_GREEN);
  }

  // Draw 60 dots
  for (int i = 0; i < 360; i += 6) {
    sx = cos((i - 90) * 0.0174532925);
    sy = sin((i - 90) * 0.0174532925);
    x0 = sx * 102 + 120;
    yy0 = sy * 102 + 120;
    // Draw minute markers
    tft.drawPixel(x0, yy0, TFT_WHITE);

    // Draw main quadrant dots
    if (i == 0 || i == 180) tft.fillCircle(x0, yy0, 2, TFT_WHITE);
    if (i == 90 || i == 270) tft.fillCircle(x0, yy0, 2, TFT_WHITE);
  }

  tft.fillCircle(120, 121, 3, TFT_WHITE);

  // Draw text at position 120,260 using fonts 4
  // Only font numbers 2,4,6,7 are valid. Font 6 only contains characters [space] 0 1 2 3 4 5 6 7 8 9 : . - a p m
  // Font 7 is a 7 segment font and only contains characters [space] 0 1 2 3 4 5 6 7 8 9 : .
  tft.drawCentreString("Time flies", 120, 260, 4);

  targetTime = millis() + 1000;


  initWiFi();

}

void loop() {
  boolean draw = false;
  static int8_t lastSec = -1;
  delay(100);
  if (timeClient.update()) {
    ss = timeClient.getSeconds();
    mm = timeClient.getMinutes();
    hh = timeClient.getHours();
    if (lastSec != ss) {
      lastSec = ss;
      Serial.println(timeClient.getFormattedTime());
      draw = true;
      char buf[64];
      sprintf(buf, "%02d:%02d:%02d, %d",
              timeClient.getHours(), timeClient.getMinutes(), timeClient.getSeconds(),
              timeClient.getDay());
      lcd.home();
      lcd.print(buf);
    }
  }  else {
    if (targetTime < millis()) {
      draw = true;
      targetTime += 1000;
      ss++;              // Advance second
      if (ss == 60) {
        ss = 0;
        mm++;            // Advance minute
        if (mm > 59) {
          mm = 0;
          hh++;          // Advance hour
          if (hh > 23) {
            hh = 0;
          }
        }
      }
    }
  }
  // return ;
  if (draw) {
    // Pre-compute hand degrees, x & y coords for a fast screen update
    sdeg = ss * 6;                // 0-59 -> 0-354
    mdeg = mm * 6 + sdeg * 0.01666667; // 0-59 -> 0-360 - includes seconds
    hdeg = hh * 30 + mdeg * 0.0833333; // 0-11 -> 0-360 - includes minutes and seconds
    hx = cos((hdeg - 90) * 0.0174532925);
    hy = sin((hdeg - 90) * 0.0174532925);
    mx = cos((mdeg - 90) * 0.0174532925);
    my = sin((mdeg - 90) * 0.0174532925);
    sx = cos((sdeg - 90) * 0.0174532925);
    sy = sin((sdeg - 90) * 0.0174532925);

    if (ss == 0 || initial) {
      initial = 0;
      // Erase hour and minute hand positions every minute
      tft.drawLine(ohx + 1, ohy + 1, 120 + 1, 121 + 1, TFT_BLACK);
      tft.drawLine(ohx, ohy, 120, 121, TFT_BLACK);
      tft.drawLine(ohx - 1, ohy - 1, 120 - 1, 121 - 1, TFT_BLACK);
      ohx = hx * 62 + 121;
      ohy = hy * 62 + 121;
      tft.drawLine(omx, omy, 120, 121, TFT_BLACK);
      tft.drawLine(omx - 1, omy - 1, 120 - 1, 121 - 1, TFT_BLACK);
      omx = mx * 84 + 120;
      omy = my * 84 + 121;
    }

    // Redraw new hand positions, hour and minute hands not erased here to avoid flicker
    tft.drawLine(osx, osy, 120, 121, TFT_BLACK);
    osx = sx * 90 + 121;
    osy = sy * 90 + 121;
    tft.drawLine(osx, osy, 120, 121, TFT_RED);
    tft.drawLine(ohx + 1, ohy + 1, 120 + 1, 121 + 1, TFT_WHITE);
    tft.drawLine(ohx, ohy, 120, 121, TFT_WHITE);
    tft.drawLine(ohx - 1, ohy - 1, 120 - 1, 121 - 1, TFT_WHITE);
    tft.drawLine(omx, omy, 120, 121, TFT_WHITE);
    tft.drawLine(omx - 1, omy - 1, 120 - 1, 121 - 1, TFT_WHITE);
    tft.drawLine(osx, osy, 120, 121, TFT_RED);

    tft.fillCircle(120, 121, 3, TFT_RED);

    char buf[64];
    sprintf(buf, "%02d:%02d:%02d, %d",
            timeClient.getHours(), timeClient.getMinutes(), timeClient.getSeconds(),
            timeClient.getDay());
    tft.drawCentreString(buf, 120, 260, 4);
  }
}


static uint8_t conv2d(const char* p) {
  uint8_t v = 0;
  if ('0' <= *p && *p <= '9')
    v = *p - '0';
  return 10 * v + *++p - '0';
}
