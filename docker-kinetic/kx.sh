xhost +
docker stop roskx
docker rm roskx

docker run -it \
    -p 11411:11411 \
    -p 11311:11311 \
    --name=roskx \
    --device=/dev/dri:/dev/dri  \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=${XAUTH}" \
    --volume=$XSOCK:$XSOCK:rw \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume=/home/chengc/ros:/root/ros_ws \
    roskx  roscore
    

#    --volume="$XAUTH:$XAUTH" \
#    -env="XAUTHORITY=$XAUTH" \
#    --env="DISPLAY=$DISPLAY" \


