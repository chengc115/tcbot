# install gazebo


# first robot

## following robot

# create scene, ... (the picture for background scene, environemnt )



# SDF

GAZEBO的世界是由SDF (Simulation Description Format)

|tag|description|attribute|text|
|:-------------|:---------------------|:----|:---|
|home|http://sdformat.org/spec?ver=1.6&elem=model#model_static|||
|/sdf|the version of sdf|version="1.4"|the file path, like model_tcbot-1_4.sdf|
|/sdf/include/uri|include resouce||model://ground_plane|
|/sdf/include/pose|6 value(xyzrpy), Override the pose of the included model. A position and orientation in the global coordinate frame for the model. Position(x,y,z) and rotation (roll, pitch yaw) in the global coordinate frame.||0 0 0 0 0 0|
|/sdf/model| the model description|name="ground_plane"||
|/sdf/model/static|  If set to true, the model is immovable. Otherwise the model is simulated in the dynamics engine.||tru or false|
|/sdf/model/link| and object |name="main_body"||
|.../link/collision|the save area, may be bigger then visual|||
|...collision/geometry|location|||
|...link/visual|can be seen in gazebo|||
|.../visual/cast_shadows||||
|.../visual/geometry||||
|.../visual/plane||||
|.../visual/material||||
|.../material/script||||
|.../material/script/uri|||file://media/materials/scripts/gazebo.material|
|.../material/script/name|||Gazebo/Grey|




```<sdf version="1.5">```


# TurtleBot

#
