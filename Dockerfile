FROM osrf/ros:kinetic-desktop-full 
# FROM osrf/ros:melodic-desktop-full

ENV DEBIAN_FRONTEND noninteractive


# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

RUN echo "source /opt/ros/kinetic/setup.bash" >> /root/.bashrc
# RUN echo "source /opt/ros/kinetic/setup.sh" >> /root/.bashrc

RUN ( . /opt/ros/kinetic/setup.sh ; rosdep init ; rosdep update )

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y apt-utils
RUN apt-get install -y dialog whiptail # librealsense2-dkms
RUN apt-get install -y zip vim apt-utils python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool
RUN apt-get install debian-keyring linux-source-4.15.0
RUN apt-get install -y ros-kinetic-rosserial 
RUN apt-get install -y ros-kinetic-kobuki-ftdi 
RUN apt-get install -y ros-kinetic-ar-track-alvar-msgs 
RUN apt-get install -y ros-kinetic-turtlebot ros-kinetic-turtlebot-apps ros-kinetic-turtlebot-interactions ros-kinetic-turtlebot-simulator || echo done 
RUN ( . /opt/ros/kinetic/setup.sh ; rosdep init ; rosdep update )

# root@68aab1f5e5a0:/# apt install system-config-printer-udev


# RUN apt-get upgrade -y
# RUN apt-get install -y zip vim apt-utils
# RUN apt-get install -y python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool 


# RUN echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc

# RUN apt-get install -y ros-kinetic-rosserial 
# RUN apt-get install -y  --ignore-missing ros-kinetic-turtlebot ros-kinetic-turtlebot-apps ros-kinetic-turtlebot-interactions ros-kinetic-turtlebot-simulator ros-kinetic-kobuki-ftdi ros-kinetic-ar-track-alvar-msgs 
# RUN echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

# ros-kinetic-rocon-remocon ros-kinetic-rocon-qt-library 

# FROM osrf/ros:kinetic-desktop-full
# nvidia-docker hooks
# LABEL com.nvidia.volumes.needed="nvidia_driver"
# ENV PATH /usr/local/nvidia/bin:${PATH}
# ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}

