
#include "Arduino.h"
#include <FreeRTOS.h>


#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS   attachInterrupt(INTERRUPT_PIN,RISING);
#include <LRTC.h>
#endif

#define U8X8_HAVE_HW_SPI
#include <U8g2lib.h>
#include <SPI.h>

// ----------------------------------
extern uint8_t SPI_LOCKED;

// ------------------------------
// static TaskHandle_t oledTaskTid = NULL;
// static uint32_t oledTaskParam = 0;
static TickType_t oledTaskDelay = 300;
// --------------------------------------
U8G2_SSD1309_128X64_NONAME2_F_4W_HW_SPI u8g2(U8G2_R0,  /* cs=*/ 16, /* dc=*/ 15, /* reset=*/ 14);

void oledTaskCode(void * const pvParam) {
  static uint32_t wakeup;
  static uint32_t taskDelay;
  static uint8_t state = 0;
  static uint8_t takeLocker = false;

  taskDelay = oledTaskDelay;
  do {
    if (takeLocker) takeLocker = SPI_LOCKED = false;
    if (pvParam) {
      wakeup = ulTaskNotifyTake( -1L,  taskDelay );
      taskDelay = oledTaskDelay;
    }
    if (SPI_LOCKED) {
      taskDelay = 3;
      continue;
    }
    takeLocker = SPI_LOCKED = true;

    switch (state) {
      case 0:
        u8g2.begin();
        u8g2.setFont(u8g2_font_ncenB08_tr);
        u8g2.enableUTF8Print();  // enable UTF8 support for the Arduino print() function
        u8g2.firstPage();
        state++;
        break;
      case 1:
        u8g2.clearBuffer();
        u8g2.print("Hello World!");
        u8g2.nextPage();
    }
  } while (pvParam != NULL);

}
