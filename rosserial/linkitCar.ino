
#include "Arduino.h"

#include <FreeRTOS.h>

#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS   attachInterrupt(INTERRUPT_PIN,RISING);
#include <LWiFi.h>
#include <LWatchDog.h>
#include <LRTC.h>
#endif

#include <WiFiUdp.h>
#include <NTPClient.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include <ros.h>
#include <geometry_msgs/Twist.h>

// -----------------------------------
uint8_t I2C_LOCKED = false;
uint8_t SPI_LOCKED = false;

/////////////////////////////////////////////////////////////

// const uint32_t wifiTaskDelay = 100;
TaskHandle_t wifiTaskTid = NULL;
uint32_t wifiTaskParam = 0;
void wifiTaskCode(void * pvParam );

// uint32_t lcdTaskDelay = 250;
TaskHandle_t lcdTaskTid = NULL;
uint32_t lcdTaskParam = 0;
void lcdTaskCode(void * pvParam );

// uint32_t oledTaskDelay = 250;
TaskHandle_t oledTaskTid = NULL;
uint32_t oledTaskParam = 0;
void oledTaskCode(void * pvParam );

// uint32_t motorTaskDelay = 10;
TaskHandle_t motorTaskTid = NULL;
uint32_t motorTaskParam = 0;
void motorTaskCode(void * pvParam );

// uint32_t mpuTaskDelay = 10;
TaskHandle_t mpuTaskTid = NULL;
uint32_t mpuTaskParam = 0;
void mpuTaskCode(void * pvParam );
////////////////////////////////////////////////////////////////////////////////////
#include "RTClib.h"
RTC_Millis rtc;

#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <LRTC.h>
#endif

void initRTC() {
  rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  DateTime now = rtc.now();
#if defined ARDUINO_ARCH_LINKIT_RTOS
  LRTC.begin();
  LRTC.set(now.year(), now.month(), now.day(),
           now.hour(), now.minute(), now.second());
#endif
}

// ---------------------------------
void initWatchDog() {
#if defined ARDUINO_ARCH_LINKIT_RTOS
  LWatchDog.begin(10);
#endif
}
void   feedWatchDog() {
#ifdef ARDUINO_ARCH_LINKIT_RTOS
  LWatchDog.feed();
#endif
}
/////////////////////////////////////////////////////////////
// #define INTERRUPT_PIN 6
// void pin_change(void) {
//
// }
//
// void initISR() {
//   pinMode(INTERRUPT_PIN, INPUT_PULLUP);
//   attachInterrupt(INTERRUPT_PIN, pin_change, RISING);
// }
/////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(115200);
  I2C_LOCKED = SPI_LOCKED = false;
  // initISR();
  initRTC();
  initWatchDog();

  // xTaskCreate(mpuTaskCode, (const char *)"mpu", 16384, (void *)&mpuTaskParam, tskIDLE_PRIORITY, &mpuTaskTid);
  xTaskCreate(wifiTaskCode, (const char *)"wifi", 4096, (void *)&wifiTaskParam, tskIDLE_PRIORITY, &wifiTaskTid);
  xTaskCreate(lcdTaskCode, (const char *)"lcd", 2048, (void *)&lcdTaskParam, tskIDLE_PRIORITY, &lcdTaskTid);

  // xTaskCreate(oledTaskCode, (const char *)"oled", 1024, (void *)&oledTaskParam, tskIDLE_PRIORITY, &oledTaskTid);
}

//////////////////////////////////////////

// xTaskCreate( lcdTask, "lcd",128,lcdParam,lcd
void loop() {
  // put your main code here, to run repeatedly:
  // Serial.print('.');
  //  oledTaskCode(NULL);
  delay(1000);
  feedWatchDog();
}
