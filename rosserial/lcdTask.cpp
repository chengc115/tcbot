#include <FreeRTOS.h>
#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS 
#endif
#include <LiquidCrystal_I2C.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include "RTClib.h"

//-----------------------------------
extern uint8_t I2C_LOCKED;
static DateTime rtcNow;
static RTC_Millis rtc;

//-----------------------------------
static const uint32_t lcdTaskDelay = 250;
// static TaskHandle_t lcdTaskTid = 0;
// static uint32_t lcdTaskParam = 0;
//------------------------------

// extern geometry_msgs::Twist rosTwist;
geometry_msgs::Twist& getTwist();
extern NTPClient timeClient;
extern double pidOutput;
extern double originalSetpoint;
extern double pidInput;
extern double setpoint;
extern double avgInput;

#ifdef ARDUINO_ARCH_LINKIT_RTOS
LiquidCrystal_I2C lcd(0x27); // set the LCD address to 0x27 for a 16 chars and 2 line display
#else
LiquidCrystal_I2C lcd(0x3f, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display
// LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display
#endif

const uint8_t lcd_bitmap[][8] =
{
  // chequer
  {0x15, 0x0A, 0x15, 0x0A, 0x15, 0x0A, 0x15, 0x00},
  // up arrow
  {0x04, 0x0E, 0x1F, 0x04, 0x04, 0x04, 0x00, 0x00},
  // down arrow
  {0x00, 0x00, 0x04, 0x04, 0x04, 0x1F, 0x0E, 0x04},
  // rectangle
  {0x00, 0x1F, 0x11, 0x11, 0x11, 0x11, 0x1F, 0x00},
  // up-left arrow
  {0x1F, 0x1E, 0x1C, 0x1A, 0x11, 0x00, 0x00, 0x00},
  // up-right arrow
  {0x1F, 0x0F, 0x07, 0x0B, 0x11, 0x00, 0x00, 0x00},
  // down-left arrow
  {0x00, 0x00, 0x00, 0x11, 0x1A, 0x1C, 0x1E, 0x1F},
  // down-right arrow
  {0x00, 0x00, 0x00, 0x11, 0x0B, 0x07, 0x0F, 0x1F},
};

//---------------------------
char lcdLine[4][32];

void lcdTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  static char buf[32];
  static uint32_t loops = 0;
  static uint8_t lockedByMe = false;
  taskDelay = lcdTaskDelay;
  Serial.println("init LCD");
  do {
    if (lockedByMe) { lockedByMe = I2C_LOCKED = false; }
    if (pvParam != NULL)
      wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    if (I2C_LOCKED) {
      taskDelay = 5;
      continue;
    }
    lockedByMe = I2C_LOCKED = true;

    taskDelay = lcdTaskDelay;
    switch (state) {
      case 0:
#ifdef ARDUINO_ARCH_LINKIT_RTOS
        lcd.begin(20, 4);
#else
        lcd.init();
#endif
        static int bitmap_size = sizeof(lcd_bitmap) / sizeof(lcd_bitmap[0]);
        for (int i = 0; i < bitmap_size; i++ )  {
          lcd.createChar(i, (uint8_t *)lcd_bitmap[i]);
        }
        lcd.noBacklight();
        state++;
        break;
      case 1:
        // lcd.begin(20, 4);
        lcd.backlight();
        state++;
        break;
      case 2:
        lcd.clear();
        for (int i = 0; i < 4; i++) {
          strcpy(lcdLine[i], "hello");
        }
        lcd.setCursor(0, 2);
        for (register int i = 0; i < 8; i++ ) {
          lcd.print(char(i));
        }

        state++; break;
      case 3:
        lcd.setCursor(0, 0);
        lcd.print(lcdLine[0]);
        state++;
        break;
      case 4:
        lcd.setCursor(0, 0);
        rtcNow = rtc.now();
        sprintf(buf, "LCD=%04d/%02d/%02d %02d:%02d:%02d\n",
                rtcNow.year(), rtcNow.month() , rtcNow.day(),
                rtcNow.hour(), rtcNow.minute(), rtcNow.second());
        Serial.print(buf);

        lcd.print(buf + 15);
        lcd.setCursor(10, 0);
        sprintf(buf, "%9d",  ++loops);
        lcd.print(buf);
        state++; break;
      case 5:
        lcd.setCursor(0, 1);
        sprintf(buf, "%9.3f", pidInput);
        lcd.print(buf);
        lcd.setCursor(10, 1);
        sprintf(buf, "%9.3f", avgInput);
        lcd.print(buf);
        state++; break;
      case 6:
        lcd.setCursor(0, 2);
        sprintf(buf, "%9.3f", pidOutput);
        lcd.print(buf);
        lcd.setCursor(10, 2);
        sprintf(buf, "%9.3f", setpoint);
        lcd.print(buf);
        state++; break;
      case 7:
        //lcd.setCursor(0, 3); lcd.printf("%6.2f %6.2f %6.2f",
        //                                CONST_PID_P, CONST_PID_I, CONST_PID_D);
        lcd.setCursor(0, 3);
        sprintf(buf, "%6.2f %6.2f %6.2f",
                getTwist().linear.x, getTwist().linear.y, getTwist().angular.z);
        lcd.print(buf);
        state = 4; break;
    }

  } while (pvParam != NULL);
}
