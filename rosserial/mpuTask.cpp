#include <Arduino.h>
#include <FreeRTOS.h>
#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS 
#endif
#include <PID_v1.h>

#define MPU6050_DMP_FIFO_RATE_DIVISOR 0x09 // defined in MPU6050_6Axis_MotionApps20.h (200/(0x09+1))Hz
#include "MPU6050_6Axis_MotionApps20.h"

#ifndef M_PI
#define M_PI (3.1415925)
#endif
#ifndef _BV
#define _BV(x) (x)
#endif

//-----------------------------------
extern uint8_t I2C_LOCKED;

//-----------------------------------
static const uint32_t mpuTaskDelay = 100;
//static TaskHandle_t mpuTaskTid = 0;
//static uint32_t mpuTaskParam = 0;

static boolean dmpReady = false;
static boolean pidInputChanged = false;
// ----------------------------------------------
MPU6050 mpu;
#define MPU_INTERRUPT_PIN 6
// --------------------------------------------
volatile uint8_t mpuInterrupt = 0;

uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[1050]; // FIFO storage buffer
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
// -----------------------------------------------
#define CONST_ORIGINAL_SETPOINT 97.28f
#define CONST_PID_P 25.0
#define CONST_PID_I 0.02
#define CONST_PID_D 0.8

double pidOutput = 0;
double originalSetpoint = (CONST_ORIGINAL_SETPOINT);
double pidInput = CONST_ORIGINAL_SETPOINT;
double setpoint = (CONST_ORIGINAL_SETPOINT);
double avgInput = 0;
PID pid(&pidInput, &pidOutput, &setpoint, CONST_PID_P, CONST_PID_I, CONST_PID_D, AUTOMATIC);
// -----------------------------------------------
#if defined(ESP32)
void IRAM_ATTR dmpDataReadyISR() {
  ++mpuInterrupt;
}
#else
void dmpDataReadyISR() {
  ++mpuInterrupt;
}
#endif

void inline setGyroOffsets() {
  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
}
// -------------------------------
void mpuTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  static uint8_t readCount = 0;
  static uint8_t lockedByMe = false;
  static char buf[80];
  taskDelay = mpuTaskDelay;

  do {
    //        Serial.print("Enter MPU... locked by others? "); Serial.print(lockedByMe);
    //        Serial.print(' '); Serial.print(I2C_LOCKED);
    //        Serial.print(' '); Serial.print(taskDelay);
    //        Serial.println();
    Serial.print('?');
    if (lockedByMe) lockedByMe = I2C_LOCKED = false;
    if (pvParam != NULL)
      wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    if (I2C_LOCKED) {
      Serial.print('F');
      taskDelay = 5;
      continue;
    }
    Serial.print('S');
    lockedByMe = I2C_LOCKED = true;

    //    Serial.print("Enter MPU... locked by others? "); Serial.print(lockedByMe);
    //    Serial.print(' '); Serial.print(I2C_LOCKED);
    //    Serial.print(' '); Serial.print(taskDelay);
    //    Serial.println();

#if defined ARDUINO_ARCH_LINKIT_RTOS
    pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
#else
    pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
#endif

    taskDelay = mpuTaskDelay;
    switch (state) {
      case 0:
        Serial.print("Entering init...: sorry no timeout, so you must make sure that the device is connected");
        if (!mpu.testConnection()) {
          Serial.println("try next ...");
          taskDelay = 1000;
          break;
        }
        Serial.println("try next ...XX");

        mpu.initialize();
        // pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
        // pinMode(MPU_INTERRUPT_PIN, INPUT_PULLDOWN);

        taskDelay = 100;
        state++;
        break;
      case 1:
        Serial.println(F("Testing device connections..."));
        Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
        Serial.println(F("\nSend any character to begin DMP programming and demo: "));
        Serial.println(F("Initializing DMP..."));
        devStatus = mpu.dmpInitialize();

        // supply your own gyro offsets here, scaled for min sensitivity
        setGyroOffsets();

        // make sure it worked (returns 0 if so)
        if (devStatus == 0) {
          // turn on the DMP, now that it's ready
          Serial.println(F("Enabling DMP..."));
          mpu.setDMPEnabled(true);

          // enable Arduino interrupt detection
          Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
#if defined(ARDUINO_ARCH_LINKIT_RTOS)
#define digitalPinToInterrupt(x) (x)
#endif
          Serial.print(digitalPinToInterrupt(MPU_INTERRUPT_PIN));
          // Serial.print((MPU_INTERRUPT_PIN));
          Serial.println(F(")..."));

          attachInterrupt(digitalPinToInterrupt(MPU_INTERRUPT_PIN), dmpDataReadyISR, RISING);
          mpuIntStatus = mpu.getIntStatus();

          // set our DMP Ready flag so the main loop() function knows it's okay to use it
          Serial.println(F("DMP ready! Waiting for first interrupt..."));
          dmpReady = true;

          // get expected DMP packet size for later comparison
          packetSize = mpu.dmpGetFIFOPacketSize();
        } else {
          // ERROR!
          // 1 = initial memory load failed
          // 2 = DMP configuration updates failed
          // (if it's going to break, usually the code will be 1)
          Serial.print(F("DMP Initialization failed (code "));
          Serial.print(devStatus);
          Serial.println(F(")"));
        }
        readCount = 0;
        state++;
      //break;
      case 2:
        if (devStatus == 0) {
          state++;
        } else {
          if (++readCount > 10) state = 0;
          break;
        }
      case 3: // check if no data...
        if (!dmpReady) {
          state = 0;
          break;
        }
        if (!mpuInterrupt) break;
        fifoCount = mpu.getFIFOCount();
        if ((fifoCount = mpu.getFIFOCount()) < packetSize) break;
        mpuIntStatus = mpu.getIntStatus();
        if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
          // overflow...
          mpu.resetFIFO();
          // fifoCount = mpu.getFIFOCount();
          sprintf(buf, "FIFO overflow: %04x, %04x, %d\n", mpuIntStatus, MPU6050_INTERRUPT_FIFO_OFLOW_BIT, fifoCount);
          Serial.print(buf);
          // Serial.println(F("FIFO overflow!"));
          // state = 0;
          break;
        }
        state++;
      case 4:
        // while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
        readCount = 0;
        while (fifoCount >= packetSize) {
          // delay(1);
          mpu.getFIFOBytes(fifoBuffer, packetSize);
          fifoCount -= packetSize;
          readCount++;
          // fifoCount = mpu.getFIFOCount();
        } // we only want the latest value

        if (fifoCount != 0) {
          mpu.resetFIFO();
          break;
        }

        static Quaternion q;           // [w, x, y, z]         quaternion container

        static VectorFloat gravity;    // [x, y, z]            gravity vector
        static float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        pidInput = (ypr[1] * (180.0f / M_PI) + 270.0f);
        if (pidInput > 360.0f)pidInput -= 360.0f;
        pidInputChanged = true;

        static uint8_t lazy = 0;
        if ( (++lazy) > 0) {
          lazy = 0;
          if (1 == 2) {
            sprintf(buf, "ypr:\t%02d\t%8.3f\t%8.3f\t%8.3f\t%8.3f\n",
                    readCount, pidInput,
                    ypr[0] * (180.0 / M_PI),
                    ypr[1] * (180.0 / M_PI),
                    ypr[2] * (180.0 / M_PI));
            Serial.print(buf);
          }
          if (1 == 2) {
            sprintf(buf, "quat: %2d, %8.3f\t%8.3f\t%8.3f\t%8.3f\t%8.3f\n", readCount, q.w, q.x, q.y, q.z, pidInput);
            Serial.print(buf);
          }
        }
    }
    Serial.println("Leave init");
    if (state >= 3) state = 3; // continue check data

  } while (pvParam != NULL);
}
