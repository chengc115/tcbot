
#include <FreeRTOS.h>
// #include <task.h>
#include <task.h>
#include <Wire.h>
/////////////////////////////////////////////////////////////

#define USER_BTN 6
/////////////////////////////////////////////////////////////
#include <Servo.h>
#define UPDOWN_PIN 5
#define LEFT_RIGHT_PIN 4

Servo servoUD;
Servo servoLR;
void initServo() {
  servoUD.attach(UPDOWN_PIN);
  servoLR.attach(LEFT_RIGHT_PIN);
}

////////////////////////////////////////////////////////////////////////////////////
#include <LRTC.h>
#include "RTClib.h"
RTC_Millis rtc;

void initRTC() {
  LRTC.begin();
  LRTC.set(2019, 8, 12, 23, 22, 30);
  rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  DateTime now = rtc.now();
  LRTC.set(now.year(), now.month(), now.day(),
           now.hour(), now.minute(), now.second());
}
////////////////////////////////////////////////////////////////////////////////////
#include <LiquidCrystal_I2C.h>
// -------------------------------------------------------------------------------
// xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, &xHandle );
TaskHandle_t lcdTaskHandle = NULL;
uint32_t lcdTaskParam = 0;
TickType_t lcdTaskDelay = 1000;
uint8_t lcdLine[4][24];


// LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27);  // set the LCD address to 0x27 for a 16 chars and 2 line display
// up to 8 bitmaps are supported
const uint8_t lcd_bitmap[][8] =
{
  // chequer
  {0x15, 0x0A, 0x15, 0x0A, 0x15, 0x0A, 0x15, 0x00},
  // up arrow
  {0x04, 0x0E, 0x1F, 0x04, 0x04, 0x04, 0x00, 0x00},
  // down arrow
  {0x00, 0x00, 0x04, 0x04, 0x04, 0x1F, 0x0E, 0x04},
  // rectangle
  {0x00, 0x1F, 0x11, 0x11, 0x11, 0x11, 0x1F, 0x00},
  // up-left arrow
  {0x1F, 0x1E, 0x1C, 0x1A, 0x11, 0x00, 0x00, 0x00},
  // up-right arrow
  {0x1F, 0x0F, 0x07, 0x0B, 0x11, 0x00, 0x00, 0x00},
  // down-left arrow
  {0x00, 0x00, 0x00, 0x11, 0x1A, 0x1C, 0x1E, 0x1F},
  // down-right arrow
  {0x00, 0x00, 0x00, 0x11, 0x0B, 0x07, 0x0F, 0x1F},
};

void lcdTaskCode(void * const pvParam) {
  static uint32_t wakeup;
  static uint8_t state = 0;
  if (state == 0) {
    state++;
    // taskENTER_CRITICAL();

    // taskEXIT_CRITICAL();
    // BaseType_t xTaskNotify( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction );

    lcd.begin(20, 4);
    lcd.backlight();
    lcd.setCursor(3, 0);
    lcd.print("Ywrobot Arduino!");
    lcdLine[0][0] = lcdLine[1][0] = lcdLine[2][0] = lcdLine[3][0] = 0;
    // register the custom bitmaps
  }
  static int bitmap_size = sizeof(lcd_bitmap) / sizeof(lcd_bitmap[0]);
  int i;
  for (i = 0; i < bitmap_size; i++ )  {
    lcd.createChar(i, (uint8_t *)lcd_bitmap[i]);
  }

  lcd.setCursor(0, 2);
  for (i = 0; i < 8; i++ ) {
    lcd.print(char(i));
  }

  do {
    static char buf[32];
    if (pvParam)
      wakeup = ulTaskNotifyTake( -1L,  lcdTaskDelay );


    LRTC.get();
    DateTime now = rtc.now();
    sprintf((char *)lcdLine[0], "%04d/%02d/%02d %02d:%02d:%02d",
            now.year(), now.month(), now.day(),
            now.hour(), now.minute(), now.second());
    for (i = 0; i < 4; i++) {
      lcdLine[i][20] = 0;
      sprintf(buf, "%-20s", lcdLine[i]);
      lcd.setCursor(0, i); lcd.print(buf);
    }
  } while ( pvParam != NULL);
}

/////////////////////////////////////////////////////////////
#define U8X8_HAVE_HW_SPI
#include <U8g2lib.h>


#include <SPI.h>
// ---------------------------------------------------------
TaskHandle_t oledTaskHandle = NULL;
uint32_t oledTaskParam = 0;
TickType_t oledTaskDelay = 300;

// U8G2_SSD1306_128X64_NONAME_F_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 13, /* data=*/ 11, /* cs=*/ 16, /* dc=*/ 15, /* reset=*/ 14);
U8G2_SSD1309_128X64_NONAME2_F_4W_HW_SPI u8g2(U8G2_R0,  /* cs=*/ 16, /* dc=*/ 15, /* reset=*/ 14);
// U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0,  /* cs=*/ 16, /* dc=*/ 15, /* reset=*/ 14);



void oledTaskCode(void * const pvParam) {
  static uint32_t wakeup;
  static uint8_t state = 0;
  if (state == 0) {
    state++;
    // taskENTER_CRITICAL();
    // taskEXIT_CRITICAL();
    // BaseType_t xTaskNotify( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction );

    // taskENTER_CRITICAL();
    u8g2.begin();
    // u8x8.setPowerSave(0);
    u8g2.setFont(u8g2_font_ncenB08_tr);
    // u8g2.setFontMode(1);  // Transparent
    // u8g2.firstPage();
    // taskEXIT_CRITICAL();
    u8g2.enableUTF8Print();    // enable UTF8 support for the Arduino print() function

    u8g2.firstPage();
  }
  do {
    static uint8_t pos = 0;
    static char buf[24];
    if (pvParam)
      wakeup = ulTaskNotifyTake( -1L,  oledTaskDelay );
servoLR.write(pos*5);
servoUD.write(pos*5);


//  u8g2.setPowerSave(pos&0x01);

    u8g2.clearBuffer();
    sprintf(buf, "OLED=%d", millis());
    Serial.println(buf);
    u8g2.drawStr(30, 10, buf);
    //   u8g2.drawBox(pos*2,pos,128-pos*4, 64-pos*2);

    //u8g2.drawHLine(pos*2,pos, 128-pos*4);
    // u8g2.drawLine(pos*2,pos,128-pos,
    //u8g2.drawTriangle(14,7, 128-pos*2,64-pos, pos*4,64-pos);
    u8g2.drawCircle(64, 32, 32 - pos);


    //    u8g2.drawVLine(pos * 2, 64 - pos, pos);
    //    u8g2.drawVLine(128 - pos * 2, 64 - pos, pos);
    // u8g2.drawBox(5,10,20,10);

    //     u8g2.drawHLine(pos*2,pos, 128-pos*2);
    //    u8g2.drawHLine(128-pos*2,pos, 128-pos*2);


    pos = (pos + 1) & 0x1f;
    //    u8g2.drawVLine(pos++,3, 40);

    //    u8g2.drawHLine(2, 35, 47);
    //    u8g2.drawHLine(3, 36, 47);
    //    u8g2.drawVLine(45, 32, 12);
    //    u8g2.drawVLine(46, 33, 12);


    //    u8g2.drawHLine(10, 100, 2);
    //    u8g2.drawHLine(3, 26, 34);
    //    u8g2.drawVLine(32, 22, 12);
    // u8g2.drawVLine(33, 23, 12);

    u8g2.setFont(u8g2_font_unifont_t_chinese2);
    // u8g2.setFont(u8g2_font_logisoso62_tn);


    u8g2.setCursor(0 + pos, 25);
    u8g2.print("Hello World!");
    u8g2.setCursor(0, 40);
    u8g2.print("世界");

    // u8g2.userInterfaceMessage("You pressed the", "Home/Quit", "Button", " Ok ");  

    //    u8g2.drawStr(0,63,"9");
    //    u8g2.drawStr(33,63,":");
    //    u8g2.drawStr(50,63,m_str);

    //taskENTER_CRITICAL();
    // u8g2.sendBuffer();
    // taskEXIT_CRITICAL();
    u8g2.nextPage();
  } while (  pvParam != NULL);
}

/////////////////////////////////////////////////////////////
void pin_change(void) {

}
//////////////////////////////////////////
#include <LWatchDog.h>

void initWatchDog() {
  LWatchDog.begin(10);
}

/////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(115200);
  attachInterrupt(USER_BTN, pin_change, RISING);

  initServo();
  initRTC();
  initWatchDog();
  xTaskCreate(lcdTaskCode, (const char *)"lcd", 1024, (void *)&lcdTaskParam, tskIDLE_PRIORITY, &lcdTaskHandle);
  xTaskCreate(oledTaskCode, (const char *)"oled", 1024, (void *)&oledTaskParam, tskIDLE_PRIORITY, &oledTaskHandle);
}



// xTaskCreate( lcdTask, "lcd",128,lcdParam,lcd

void loop() {
  // put your main code here, to run repeatedly:

  //  oledTaskCode(NULL);
  delay(1000);
  LWatchDog.feed();

}
