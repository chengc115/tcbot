xhost +
docker stop roskx
docker rm roskx
nvidia-docker run -it \
    -p 11411:11411 \
    --name=roskx \
    --device=/dev/dri:/dev/dri  \
    --env="DISPLAY=$DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    -env="XAUTHORITY=$XAUTH" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="$XAUTH:$XAUTH" \
    --volume=/home/ros:/root/ros_ws \
    --runtime=nvidia \
    roskx 

#    roscore
# export containerId=$(docker ps -l -q| grep roskx)
# echo ROS : $containerId


# sudo apt-get install ros-indigo-rosserial-arduino
# sudo apt-get install ros-indigo-rosserial

#docker run -it \
#    --name=ros \
#    --env="DISPLAY" \
#    --env="QT_X11_NO_MITSHM=1" \
#    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
#    osrf/ros:melodic-desktop-full \
#    rqt




#nvidia-docker run -it \
#    --name=ros \
#    --device=/dev/dri:/dev/dri  \
#    --env="DISPLAY" \
#    --env="QT_X11_NO_MITSHM=1" \
#    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
#    ros:melodic-desktop-full \
#    roscore

# docker run --rm -it --runtime=nvidia --privileged --net=host --ipc=host \
# -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY \
# -v $HOME/.Xauthority:/home/$(id -un)/.Xauthority -e XAUTHORITY=/home/$(id -un)/.Xauthority \
# -e DOCKER_USER_NAME=$(id -un) \
# -e DOCKER_USER_ID=$(id -u) \
# -e DOCKER_USER_GROUP_NAME=$(id -gn) \
# -e DOCKER_USER_GROUP_ID=$(id -g) \
# -e ROS_IP=localhost \
# turlucode/ros-indigo:nvidia
