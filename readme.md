#  專案說明
本專案透過ROS環境, 𨖫行建構在ARDIINO開發版上的玩具車, 以做為瞭解ROS的入門

專案中引用 GOZEBOT模擬器, 做為學習如何控制ROBOT的開端, 藉由ROS去操偏玩具車做為專案案的開端, 以瞭解ROS在開發時需要注意到的各項環節

為求安裝的順暢度, 採用了
DOKCER 安裝ROS環境, 直接使用並建構自己的ROS系統, 以節省研究的時間

如想透過虛擬器, 如VIRTUALBOX, VMWARE進行安裝,及如何從UBUNTUOS安裝完成後再加上ROS系統, 會在撰寫 DOKCERFILE一節中附加說明

# 主機基環境需求:
+ UBUNTU 18.04  LTS或Debian , 
    + 不建議使用太新版本,　容易發生Package相依性問題, 導致需要重新安D裝
+ 請勿在WINDOWS下使用DOCER方法安裝, 
    + 雖DOKCER可以在WINDOWS上執行, 但仍雵要VIRTUALBOX或開啟 HYPER-THREAD支援, 
    + 安裝不慎, WINDOWS需重新安裝
## 在 LINUX/MAC下 安裝DOCKER (模擬器)

+ 如果不是最新版的愛好者: sudo apt-get install docker.io
+ 最新版愛好者, 可以在 https://docs.docker.com/install/linux/docker-ce/ubuntu/ 找到安裝說明
 + 安裝後, 確認是否成功 :  sudo docker start hello-world

## DOCER小建議
+ 修改 /etc/group, 把常用的USER加入到DOKCER GROUP, 方便使用DOCKER
    + GOOGLE SEARCH: linux add user to group
    + $sudo usermod -a -G docker $(whoami)
        
        結果 如下: /etc/group中看到 docker:x:999:chengc

## DOKCER 小CMMAND

當透過docker 去執行
一個docker image (含tabName) , docker會先在本機中查看該IMAGE是否己存在, 如果沒有, 會再從DOCKERHUB網站下載，並存入 LOCAL HD , 並供日後使用， 直到被刪除為止（docker rmi image_name), 
當要實際執行一台機器時， docker 會自動建一個名為 （由 --name 參數指定）或自動建立UNIQUE名稱的執行容器(container))。 
建立完成後，日後就在這個container 的RUNNING SPACE下執行該系統.

+ 下載  : docker pull imag_name:tag 
+ 啟動 :  docker **run** --name container_name  image_name:tag
+  現有多少機器容器被執行(查看continer_name)): docker ps 
+  有多少機器容器被建立(查看continer_name)): docker ps  -a
+ 刪除機器容器(container) : docker rm continer_name
+ 己下載多少機器IMAGE/image_name: docker images
+ 刪除己下載的IMAGE: docker rmi image_name
+ 停止執行中的container: docker stop container_name
+ 啟動被停止的container: docker **start** container_name
+ 在執行中的container中進入console: doker **exec** -it container_name **/bin/bash**

進階參數:(參閱執行的SMAPLE SHELL)
+  在建立container時, 可以指定的參數
+ -v ; --volumn : 指定HIST與CONTAINER間檔案的MAPPING, 通常使用在把CONTAINER中開發的程式MAPPING出來, 以免未來誤刪時隨著container一起被刪
+  -p; --port : 把TCPIP PORT做MAP, 到主機的PORT, 以利從外界跟container相連
+ -e; --env : 設定container的環境變數, 通常要到 docker-hub 上看才可以找到有那些參數可以設

## 下載專案己安裝好的IMAGE

打開terminal終端機

    docker pull  osrf/ros:kinetic-desktop-full
    docker run --name  rosk osrf/ros:kinetic-desktop-full roscore

或直接執行(會自動下載)

    docker run --name  rosk osrf/ros:kinetic-desktop-full roscore

可再打另一個終端機,即可確認是否完成

    docker exec -it rosk /bin/bash

註:rviz此時無法執行,　因為Ｘ11 尚未連結

## 開啟虛擬機:

可用以下方式開啟,　或建立shell scrip (kx.sh) 供日後使用

    xhost +
    docker stop roskx
    docker rm roskx
    nvidia-docker run -it \
        -p 11411:11411 \
        --name=roskx \
        --device=/dev/dri:/dev/dri  \
        --env="DISPLAY=$DISPLAY" \
        --env="QT_X11_NO_MITSHM=1" \
        -env="XAUTHORITY=$XAUTH" \
        --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
        --volume="$XAUTH:$XAUTH" \
        --volume=/home/ros:/root/ros_ws \
        --runtime=nvidia \
        roskx

+ xhost　+: 將X Server(就是畫面/desktop)讓任何人的程式都可以顯示在畫面上
+ 透過docker command 建立一台機器, 以下參數需依個人進行修改
    + 機器名稱: --name roskx
    + 把檔案MAP到HOST來,: --volume: /home/ros:/opt/ors/ros_ws
    +  把網路MAP出來, 供rosSerial連線:  -p 11411:11411 \

### 測試
        docker exec -it container_name /binbash
        suorce /opt/ros/kinetic/setup.bash
        rviz

### 結束
        docker stop container_name

### 重新執行
        docker start container_name

### 移除(container)
        docker rm container_name

### 移除IMAGE
        docker rm container_name

### 主機重開機
 > 在重開機後, 因X11的連結會失效,  conainer 必需重新建立(xhost .....之後的命令))
 > docker start container_name

### 使用NVIDIA顯卡
    需以 nvidia-docker 取代docker即可

# 專案所需環境建立

# 重新建立一個IMAGE, for 個人使用
專案因引用網路上的 desktop-full 環境, 無法保存本專案需要自行加入的PACKAGE, 所以需透過DOKCERFILE建立本專案所需的IMAGE, 參考Dockerfile

檔案中引用 ros-kinetic-rosserial , ros-kinetic-turtlebot, 並設定NVIDIA的連結, 

檔案以為程式內容, COPY並建立到檔案 Dockerfile 後, 在該目錄下執行

        docker build . -t{imageName}
        EX: docker build . -troskx

用以産出 名為 roskx 的IMAGE, 可以使用

        docker images 
查詢

```
# copy these code to file named Dockerfile, then run docker build . -tSomeName
FROM osrf/ros:kinetic-desktop-full 
# FROM osrf/ros:melodic-desktop-full

ENV DEBIAN_FRONTEND noninteractive


# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

RUN echo "source /opt/ros/kinetic/setup.bash" >> /root/.bashrc
# RUN echo "source /opt/ros/kinetic/setup.sh" >> /root/.bashrc

RUN ( . /opt/ros/kinetic/setup.sh ; rosdep init ; rosdep update )

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y apt-utils
RUN apt-get install -y dialog whiptail # librealsense2-dkms
RUN apt-get install -y zip vim apt-utils python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool
RUN apt-get install debian-keyring linux-source-4.15.0
RUN apt-get install -y ros-kinetic-rosserial 
RUN apt-get install -y ros-kinetic-kobuki-ftdi 
RUN apt-get install -y ros-kinetic-ar-track-alvar-msgs 
RUN apt-get install -y ros-kinetic-turtlebot ros-kinetic-turtlebot-apps ros-kinetic-turtlebot-interactions ros-kinetic-turtlebot-simulator || echo done 
RUN ( . /opt/ros/kinetic/setup.sh ; rosdep init ; rosdep update )

# root@68aab1f5e5a0:/# apt install system-config-printer-udev


# RUN apt-get upgrade -y
# RUN apt-get install -y zip vim apt-utils
# RUN apt-get install -y python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool 


# RUN echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc

# RUN apt-get install -y ros-kinetic-rosserial 
# RUN apt-get install -y  --ignore-missing ros-kinetic-turtlebot ros-kinetic-turtlebot-apps ros-kinetic-turtlebot-interactions ros-kinetic-turtlebot-simulator ros-kinetic-kobuki-ftdi ros-kinetic-ar-track-alvar-msgs 
# RUN echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

# ros-kinetic-rocon-remocon ros-kinetic-rocon-qt-library 

# FROM osrf/ros:kinetic-desktop-full
# nvidia-docker hooks
# LABEL com.nvidia.volumes.needed="nvidia_driver"
# ENV PATH /usr/local/nvidia/bin:${PATH}
# ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}


```

##  執行docker
    xhost +
    docker stop roskx
    docker rm roskx
    nvidia-docker run -it \
        -p 11411:11411 \
        --name=roskx \
        --device=/dev/dri:/dev/dri  \
        --env="DISPLAY=$DISPLAY" \
        --env="QT_X11_NO_MITSHM=1" \
        -env="XAUTHORITY=$XAUTH" \
        --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
        --volume="$XAUTH:$XAUTH" \
        --volume=/home/ros:/root/ros_ws \
        --runtime=nvidia \
        roskx

## 測試
        同上

